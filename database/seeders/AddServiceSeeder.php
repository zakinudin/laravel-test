<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AddServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
                'name' => 'Cash',
            ],
            [
                'name' => 'Mobile Transfer',
            ],
            [
                'name' => 'Bank Deposit',
            ]
        ]);
    }
}
