<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AddProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('providers')->insert([
            [
                'parent_id' => null,
                'name' => 'Agro Fresh',
                'country_iso' => null,
                'service_id' => null,
                'area' => null
            ],
            [
                'parent_id' => null,
                'name' => 'BAKAAL',
                'country_iso' => null,
                'service_id' => null,
                'area' => null
            ],
            [
                'parent_id' => null,
                'name' => 'Juba',
                'country_iso' => null,
                'service_id' => null,
                'area' => null
            ],
            [
                'parent_id' => null,
                'name' => 'Capital',
                'country_iso' => null,
                'service_id' => null,
                'area' => null
            ],
            [
                'parent_id' => null,
                'name' => 'Terra',
                'country_iso' => null,
                'service_id' => null,
                'area' => null
            ],
            [
                'parent_id' => 1,
                'name' => 'Agro Fresh LTD',
                'country_iso' => 'ID',
                'service_id' => 1,
                'area' => 'Asia'
            ],
            [
                'parent_id' => 2,
                'name' => 'BAKAAL Cash',
                'country_iso' => 'UK',
                'service_id' => 1,
                'area' => 'Europe'
            ],
            [
                'parent_id' => 3,
                'name' => 'Juba Payment',
                'country_iso' => 'ID',
                'service_id' => 3,
                'area' => 'Asia'
            ],
            [
                'parent_id' => 2,
                'name' => 'BAKAAL Cash',
                'country_iso' => 'ID',
                'service_id' => 1,
                'area' => 'Asia'
            ],
            [
                'parent_id' => 5,
                'name' => 'Terra Payment',
                'country_iso' => 'US',
                'service_id' => 2,
                'area' => 'America'
            ],
            [
                'parent_id' => 3,
                'name' => 'Juba Bank',
                'country_iso' => 'SG',
                'service_id' => 3,
                'area' => 'Asia'
            ],
            [
                'parent_id' => 3,
                'name' => 'Juba Mobile Transfer',
                'country_iso' => 'ID',
                'service_id' => 2,
                'area' => 'Asia'
            ],
        ]);
    }
}
