<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AddCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('country')->insert([
            [
                'country_iso' => 'ID',
                'name' => 'Indonesia',
            ],
            [
                'country_iso' => 'GB',
                'name' => 'United Kingdom',
            ],
            [
                'country_iso' => 'US',
                'name' => 'United States',
            ],
            [
                'country_iso' => 'SG',
                'name' => 'Singapore',
            ]
        ]);
    }
}
